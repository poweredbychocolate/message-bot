package com.crystalofchocolate.services;

import com.crystalofchocolate.unit.DictionaryUnit;
import lombok.extern.log4j.Log4j2;

import java.io.Closeable;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The Dictionary schedule service, scheduled executor for {@link DictionaryUnit}.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-23
 */
@Log4j2
public class DictionaryScheduleService implements Closeable {
   private final ScheduledExecutorService executorService;

   /**
    * Instantiates a new Dictionary schedule service.
    */
   public DictionaryScheduleService() {
      executorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
      log.info("DictionaryScheduleService online");
   }

   @Override
   public void close() {
      if (!executorService.isShutdown()) {
         executorService.shutdown();
         log.info("shutdown DictionaryScheduleService");
      }
   }

   /**
    * Append {@link DictionaryUnit} list to schedule.
    *
    * @param dictionaryUnits the {@link DictionaryUnit} list
    */
   public void append(List<DictionaryUnit> dictionaryUnits) {
      dictionaryUnits.forEach(dictionaryUnit -> {
         long period = dictionaryUnit.scheduledPeriod();
         TimeUnit unit = dictionaryUnit.scheduledPeriodUnit();
         executorService.scheduleAtFixedRate(dictionaryUnit, period, period, unit);
      });
      log.info("DictionaryScheduleService, appended " + dictionaryUnits.size() + " items");
      log.info(executorService.toString());
   }
}
