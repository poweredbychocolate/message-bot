package com.crystalofchocolate.services;

import com.crystalofchocolate.unit.ValueListUnit;
import lombok.extern.log4j.Log4j2;

import java.io.Closeable;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The List schedule service, scheduled executor for {@link ValueListUnit}.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-23
 */
@Log4j2
public class ListScheduleService implements Closeable {
   private final ScheduledExecutorService executorService;

   /**
    * Instantiates a new List schedule service.
    */
   public ListScheduleService() {
      executorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
      log.info("ListScheduleService online");
   }

   @Override
   public void close() {
      if (!executorService.isShutdown()) {
         executorService.shutdown();
         log.info("shutdown ListScheduleService");
      }
   }

   /**
    * Append {@link ValueListUnit} list to schedule.
    *
    * @param valueListUnits the {@link ValueListUnit} list;
    */
   public void append(List<ValueListUnit> valueListUnits) {
      valueListUnits.forEach(valueListUnit -> {
         long period = valueListUnit.scheduledPeriod();
         TimeUnit unit = valueListUnit.scheduledPeriodUnit();
         executorService.scheduleAtFixedRate(valueListUnit, period, period, unit);
      });
      log.info("ListScheduleService, appended " + valueListUnits.size() + " items");
      log.info(executorService.toString());
   }
}
