package com.crystalofchocolate.services;

import com.crystalofchocolate.config.CoreConfig;
import com.crystalofchocolate.listeners.GlobalListener;
import com.crystalofchocolate.listeners.ChannelMessage;
import com.crystalofchocolate.loaders.DictionaryEntityLoader;
import com.crystalofchocolate.loaders.ValuesListEntityLoader;
import com.crystalofchocolate.types.DictionaryEntity;
import com.crystalofchocolate.types.ValuesListEntity;
import com.crystalofchocolate.unit.ChannelUnit;
import com.crystalofchocolate.unit.DictionaryUnit;
import com.crystalofchocolate.unit.ValueListUnit;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.Channel;
import org.javacord.api.entity.channel.ChannelType;
import org.javacord.api.entity.channel.ServerTextChannel;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Discord service.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-12
 */
@Log4j2
public class DiscordService {
   private final DiscordApi discordApi;
   private final ListScheduleService listScheduleService;
   private final DictionaryScheduleService dictionaryScheduleService;

   /**
    * Instantiates a new Discord service.
    *
    * @param coreConfig the core config
    */
   public DiscordService(CoreConfig coreConfig) {

      try {
         discordApi = new DiscordApiBuilder().setToken(coreConfig.token()).login().join();

      } catch (Exception e) {
         log.error("Cannot connect to discord api ", e);
         throw new RuntimeException("Cannot connect to discord api");
      }
      List<Optional<ServerTextChannel>> channels = discordApi.getChannels().stream()
            .filter(channel -> channel.getType().equals(ChannelType.SERVER_TEXT_CHANNEL))
            .map(Channel::asServerTextChannel)
            .filter(textChannel -> textChannel.isPresent() && textChannel.get().canYouWrite())
            .collect(Collectors.toList());

      if (channels.isEmpty()) {
         log.error("Any channel is not available");
         throw new RuntimeException("Any channel is not available");
      }
      ChannelUnit channelUnit = new ChannelUnit(channels);
      ChannelMessage channelMessage = new ChannelMessage(channelUnit);

      GlobalListener globalListener = new GlobalListener(channelMessage);
      discordApi.addMessageCreateListener(globalListener);

      listScheduleService = new ListScheduleService();
      List<ValuesListEntity> valuesListEntities = new ValuesListEntityLoader().loadValuesListEntries(coreConfig.listFilesPath());

      List<ValueListUnit> valueListUnits = valuesListEntities.stream()
            .map(entity -> new ValueListUnit(entity, new ChannelUnit(channels)))
            .collect(Collectors.toList());

      listScheduleService.append(valueListUnits);

      dictionaryScheduleService = new DictionaryScheduleService();
      List<DictionaryEntity> dictionaryEntities = new DictionaryEntityLoader().loadDictionaryEntries(coreConfig.dictionaryFilesPath());

      List<DictionaryUnit> dictionaryUnits = dictionaryEntities.stream()
            .map(entity -> new DictionaryUnit(entity, new ChannelUnit(channels)))
            .collect(Collectors.toList());

      dictionaryScheduleService.append(dictionaryUnits);
      log.info("DiscordService online");
   }

   /**
    * Stop and disconnect from discord bot.
    */
   public void stop() {
      listScheduleService.close();
      dictionaryScheduleService.close();
      discordApi.disconnect();
   }
}
