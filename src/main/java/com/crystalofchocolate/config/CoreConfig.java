package com.crystalofchocolate.config;

import com.crystalofchocolate.types.Token;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * The Core config, app configuration.
 *
 * @author Dawid
 * @version 1
 * @since 2020 -07-12
 */
@RequiredArgsConstructor
@Builder
public class CoreConfig {
   private final Token token;
   private final List<String> listFilesPath;
   private final List<String> dictionaryFilesPath;

   /**
    * Get discord apps token.
    *
    * @return the token string
    */
   public String token() {
      return token.get();
   }

   /**
    * Get list of file patches.
    *
    * @return the list
    */
   public List<String> listFilesPath() {
      return listFilesPath;
   }

   public List<String> dictionaryFilesPath() {
      return dictionaryFilesPath;
   }
}
