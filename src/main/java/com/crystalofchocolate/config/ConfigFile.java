package com.crystalofchocolate.config;

import com.crystalofchocolate.types.Token;
import lombok.Data;

import java.util.List;

/**
 * The type Config file.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-12
 */
@Data
public
class ConfigFile {
   private String token;
   private List<String> enabled_list_files;
   private List<String> enabled_dictionary_files;

   /**
    * Map file config to {@link CoreConfig}.
    *
    * @return the {@link CoreConfig} instance
    */
   public CoreConfig as() {
      return CoreConfig.builder()
            .token(new Token(token))
            .listFilesPath(enabled_list_files)
            .dictionaryFilesPath(enabled_dictionary_files)
            .build();
   }
}
