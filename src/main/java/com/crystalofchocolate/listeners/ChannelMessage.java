package com.crystalofchocolate.listeners;

import com.crystalofchocolate.unit.ChannelUnit;
import lombok.RequiredArgsConstructor;

/**
 * The type Channel message service,send message called via command.
 *
 * @author Dawid
 * @version 1
 * @since 2020 -07-12
 */
@RequiredArgsConstructor
public class ChannelMessage {
   private final ChannelUnit channelUnit;

   /**
    * Get random channel and send message.
    *
    * @param message the message
    */
   public void send(String message) {
      channelUnit.sendMessage(message);
   }

   /**
    * Find channel by id and send message.
    *
    * @param channelId the channel id
    * @param message   the message
    */
   public void send(long channelId, String message) {
      channelUnit.sendMessage(channelId, message);
   }

   /**
    * Find channel with given name and send message.
    *
    * @param channelName the channel name
    * @param message     the message
    */
   public void send(String channelName, String message) {
      channelUnit.sendMessage(channelName, message);
   }

}
