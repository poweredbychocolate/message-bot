package com.crystalofchocolate.listeners;

import com.crystalofchocolate.types.MessageCommand;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;

import java.time.LocalDateTime;

/**
 * The type Global listener, app main incoming message listener.
 * Distribute message to dedicated services.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-12
 */
@Log4j2
public class GlobalListener implements MessageCreateListener {
   private final ChannelMessage channelMessage;

   public GlobalListener(ChannelMessage channelMessage) {
      this.channelMessage = channelMessage;
      log.info("GlobalListener online at "+ LocalDateTime.now());
   }

   @Override
   public void onMessageCreate(MessageCreateEvent messageCreateEvent) {
      //sent message manually via private message command
      if (messageCreateEvent.isPrivateMessage()) {
         MessageCommand messageCommand = new MessageCommand(messageCreateEvent.getMessageContent());
         if (messageCommand.channel().isPresent()) {
            try {
               long parsedChannelId = Long.parseLong(messageCommand.channel().get());
               //send message to channel with given id
               channelMessage.send(parsedChannelId, messageCommand.message());
            } catch (NumberFormatException e) {
               //if channel is not id find channel by name
               channelMessage.send(messageCommand.channel().get(), messageCommand.message());
            }
         } else {
            //message on random channel
            channelMessage.send(messageCommand.message());
         }
      }
   }
}
