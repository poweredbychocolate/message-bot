package com.crystalofchocolate.loaders;

import com.crystalofchocolate.types.ValuesListEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.log4j.Log4j2;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * The Values list entity loader, load {@link ValuesListEntity} from json file.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-23
 */
@Log4j2
public class ValuesListEntityLoader {
   private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

   /**
    * Load single {@link ValuesListEntity}.
    *
    * @param listEntityPath the list entity path
    * @return the values list entity
    */
   public ValuesListEntity loadValuesListEntity(String listEntityPath) {
      try (Reader reader = new FileReader(listEntityPath)) {
         return gson.fromJson(reader, ValuesListEntity.class);
      } catch (IOException e) {
         log.error("Cannot load values list from file :: " + listEntityPath + " :: " , e);
         return null;
      }
   }

   /**
    * Load list of {@link ValuesListEntity}.
    *
    * @param listEntityPaths the list entity paths
    * @return the list
    */
   public List<ValuesListEntity> loadValuesListEntries(List<String> listEntityPaths) {
      if (Objects.isNull(listEntityPaths) || listEntityPaths.isEmpty()) {
         log.warn("Cannot load values list from empty path list");
         return Collections.emptyList();
      }
      List<ValuesListEntity> valuesListEntityList = new ArrayList<>();

      for (String path : listEntityPaths) {
         ValuesListEntity valuesListEntity = loadValuesListEntity(path);
         if (Objects.nonNull(valuesListEntity)) {
            valuesListEntityList.add(valuesListEntity);
         }
      }
      return valuesListEntityList;
   }
}
