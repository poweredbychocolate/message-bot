package com.crystalofchocolate.loaders;

import com.crystalofchocolate.types.DictionaryEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.log4j.Log4j2;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * The Dictionary entity loader, load {@link DictionaryEntity} from json file.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-23
 */
@Log4j2
public class DictionaryEntityLoader {
   private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

   /**
    * Load {@link DictionaryEntity} .
    *
    * @param dictionaryPath the dictionary path
    * @return the dictionary entity
    */
   public DictionaryEntity loadDictionaryEntity(String dictionaryPath) {
      try (Reader reader = new FileReader(dictionaryPath)) {
         return gson.fromJson(reader, DictionaryEntity.class);
      } catch (IOException e) {
         log.error("Cannot load dictionary from file :: " + dictionaryPath + " :: " , e);
         return null;
      }
   }

   /**
    * Load {@link DictionaryEntity} list.
    *
    * @param dictionaryPaths the dictionary paths
    * @return the list
    */
   public List<DictionaryEntity> loadDictionaryEntries(List<String> dictionaryPaths) {
      if (Objects.isNull(dictionaryPaths) || dictionaryPaths.isEmpty()) {
         log.warn("Cannot load dictionaries from empty path list");
         return Collections.emptyList();
      }
      List<DictionaryEntity> dictionaryEntities = new ArrayList<>();

      for (String path : dictionaryPaths) {
         DictionaryEntity dictionaryEntity = loadDictionaryEntity(path);
         if (Objects.nonNull(dictionaryEntity)) {
            dictionaryEntities.add(dictionaryEntity);
         }
      }
      return dictionaryEntities;
   }
}
