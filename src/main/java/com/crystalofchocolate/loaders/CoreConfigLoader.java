package com.crystalofchocolate.loaders;

import com.crystalofchocolate.config.ConfigFile;
import com.crystalofchocolate.config.CoreConfig;
import com.crystalofchocolate.types.FileName;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.Reader;


/**
 * The type Core config loader.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-12
 */
@Log4j2
public class CoreConfigLoader {
   private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

   /**
    * Load {@link CoreConfig} from json file.
    *
    * @param fileName the {@link FileName}
    * @return the core config
    */
   public CoreConfig loadCoreConfig(FileName fileName) {

      try (Reader reader = fileName.asFileReader()) {
         return gson.fromJson(reader, ConfigFile.class).as();
      } catch (IOException e) {
         log.error("Cannot load core config from file ::" , e);
         return null;
      }
   }
}
