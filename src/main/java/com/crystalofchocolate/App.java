package com.crystalofchocolate;

import com.crystalofchocolate.config.CoreConfig;
import com.crystalofchocolate.loaders.CoreConfigLoader;
import com.crystalofchocolate.services.DiscordService;
import com.crystalofchocolate.types.FileName;
import lombok.extern.log4j.Log4j2;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Scanner;

/**
 * Message Bot App main application class.
 *
 * @version 1
 * @since 2020-07-12
 */
@Log4j2
public class App {
   /**
    * The entry point of application.
    *
    * @param args the input arguments
    */
   public static void main(String[] args) {
      System.out.println("Message Bot");
      final FileName fileName = getFileName(args);

      final CoreConfig coreConfig = new CoreConfigLoader().loadCoreConfig(fileName);
      final DiscordService discordService = new DiscordService(coreConfig);

      log.info("Message Bot was started at " + LocalDateTime.now());
      Scanner scanner = new Scanner(System.in);
      while (true) {
         if (scanner.next().equalsIgnoreCase("exit")) break;
      }
      discordService.stop();
      log.info("Message Bot was closed at " + LocalDateTime.now());
   }

   private static FileName getFileName(String[] args) {
      if (Objects.nonNull(args) && args.length > 0)
         return new FileName(args[0]);

      return new FileName("conf.json");
   }
}
