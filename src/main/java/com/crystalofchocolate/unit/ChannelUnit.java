package com.crystalofchocolate.unit;

import org.javacord.api.entity.channel.ServerTextChannel;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * The type Channel unit, allow send messages.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-12
 */
public class ChannelUnit {
   private final List<Optional<ServerTextChannel>> allChannels;
   private final List<Optional<ServerTextChannel>> randomChannels;
   private final Random random = new Random();

   /**
    * Instantiates a new Channel unit.
    *
    * @param allChannels the all channels
    */
   public ChannelUnit(List<Optional<ServerTextChannel>> allChannels) {
      this.allChannels = allChannels;
      randomChannels = new LinkedList<>(allChannels);
   }

   /**
    * Send message on random channel.
    *
    * @param message the message
    */
   public void sendMessage(String message) {
      if (randomChannels.size() == 0) {
         randomChannels.addAll(allChannels);
      }

      Optional<ServerTextChannel> randomChannel = randomChannels.get(random.nextInt(randomChannels.size()));
      randomChannel.ifPresent(channel -> channel.sendMessage(message));
      randomChannels.remove(randomChannel);
   }

   /**
    * Send message to channel founded by id.
    *
    * @param channelId the channel id
    * @param message   the message
    */
   public void sendMessage(long channelId, String message) {
      Optional<ServerTextChannel> foundChannel = allChannels.stream()
            .filter(channel -> channel.isPresent() && channel.get().getId() == channelId)
            .map(Optional::get)
            .findFirst();

      foundChannel.ifPresent(channel -> channel.sendMessage(message));
   }

   /**
    * Send message to channel founded by name.
    *
    * @param channelName the channel name
    * @param message     the message
    */
   public void sendMessage(String channelName, String message) {
      Optional<ServerTextChannel> foundChannel = allChannels.stream()
            .filter(channel -> channel.isPresent() && channel.get().getName().equals(channelName))
            .map(Optional::get)
            .findFirst();

      foundChannel.ifPresent(channel -> channel.sendMessage(message));
   }

}
