package com.crystalofchocolate.unit;

import java.util.concurrent.TimeUnit;

/**
 * The type Entity period unit,
 * helper class for {@link ValueListUnit} and {@link DictionaryUnit} period value unit.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-23
 */
public class EntityPeriodUnit {
   private static final String MS = "minutes";
   private static final String M = "minute";
   private static final String H = "hour";
   private static final String HS = "hours";
   private static final String S = "second";
   private static final String SS = "seconds";
   private static final String D = "day";
   private static final String DS = "days";

   /**
    * Validate if unit has correct value.
    *
    * @param unit the unit string
    * @return the true if is a correct value
    */
   public static boolean validate(String unit) {
      String s = unit.toLowerCase();
      return s.equals(MS)
            || s.equals(M)
            || s.equals(H)
            || s.equals(HS)
            || s.equals(S)
            || s.equals(SS)
            || s.equals(D)
            || s.equals(DS);
   }

   /**
    * Convert unit string to {@link TimeUnit}.
    *
    * @param unit the unit string
    * @return the time unit
    */
   public static TimeUnit asTimeUnit(String unit) {
      switch (unit.toLowerCase()) {
         case MS:
         case M:
            return TimeUnit.MINUTES;
         case HS:
         case H:
            return TimeUnit.HOURS;
         case SS:
         case S:
            return TimeUnit.SECONDS;
         case D:
         case DS:
            return TimeUnit.DAYS;
      }
      return TimeUnit.MINUTES;
   }
}
