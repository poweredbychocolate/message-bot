package com.crystalofchocolate.unit;

import com.crystalofchocolate.types.DictionaryEntity;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * The type Dictionary runnable unit.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-23
 */
public class DictionaryUnit implements Runnable {
   private final DictionaryEntity dictionaryEntity;
   private final ChannelUnit channelUnit;

   private List<String> channelsToUse;
   private List<String> randomMessageKeys;
   private final Random random = new Random();

   /**
    * Instantiates a new Dictionary unit.
    *
    * @param dictionaryEntity the dictionary entity
    * @param channelUnit      the channel unit
    */
   public DictionaryUnit(DictionaryEntity dictionaryEntity, ChannelUnit channelUnit) {
      this.dictionaryEntity = dictionaryEntity;
      this.channelUnit = channelUnit;
      initChannelToUseList();
      initRandomMessageKeys();
   }

   /**
    * Scheduled period value.
    *
    * @return the period value
    */
   public long scheduledPeriod() {
      return dictionaryEntity.getPeriod();
   }

   /**
    * Scheduled period value unit.
    *
    * @return the {@link TimeUnit}
    */
   public TimeUnit scheduledPeriodUnit() {
      return EntityPeriodUnit.asTimeUnit(dictionaryEntity.getUnit());
   }

   @Override
   public void run() {
      String message = getRandomMessage();

      if (channelsToUse.isEmpty()) {
         channelUnit.sendMessage(message);
      } else {
         Optional<String> optionalChannelStringValue = getRandomChannelFromUserList();
         optionalChannelStringValue.ifPresent(channelStringValue -> {
            try {
               long parsedChannelId = Long.parseLong(channelStringValue);
               //send message to channel with given id
               channelUnit.sendMessage(parsedChannelId, message);
            } catch (NumberFormatException e) {
               //if channel is not id find channel by name
               channelUnit.sendMessage(channelStringValue, message);
            }
         });
      }
   }

   private String getRandomMessage() {
      StringBuilder stringBuilder = new StringBuilder();

      if (Objects.nonNull(dictionaryEntity.getTitle())) {
         stringBuilder.append(dictionaryEntity.getTitle());
         stringBuilder.append(System.lineSeparator());
      }

      String randomMessageKey = getRandomMessageKey();
      String randomMessageValue;
      try {
         randomMessageValue = dictionaryEntity.getValues().getOrDefault(randomMessageKey, " ? ");
      } catch (Exception e) {
         randomMessageValue = " ? ";
      }

      stringBuilder.append(randomMessageKey);
      stringBuilder.append(" - ");
      stringBuilder.append(randomMessageValue);

      return stringBuilder.toString();
   }

   private String getRandomMessageKey() {
      if (randomMessageKeys.isEmpty()) {
         initRandomMessageKeys();
      }

      String messageKey = randomMessageKeys.get(random.nextInt(randomMessageKeys.size()));
      randomMessageKeys.remove(messageKey);
      return messageKey;
   }

   private Optional<String> getRandomChannelFromUserList() {
      if (channelsToUse.isEmpty()) {
         initChannelToUseList();
      }
      if (!channelsToUse.isEmpty()) {
         String channel = channelsToUse.get(random.nextInt(channelsToUse.size()));
         channelsToUse.remove(channel);
         return Optional.of(channel);
      }
      return Optional.empty();
   }

   private void initRandomMessageKeys() {
      randomMessageKeys = Objects.nonNull(dictionaryEntity.getValues())
            ? new ArrayList<>(dictionaryEntity.getValues().keySet())
            : new ArrayList<>(Collections.singletonList(" ? "));
   }

   private void initChannelToUseList() {
      channelsToUse = Objects.nonNull(dictionaryEntity.getChannels())
            ? new ArrayList<>(dictionaryEntity.getChannels())
            : new ArrayList<>(Collections.emptyList());
   }
}
