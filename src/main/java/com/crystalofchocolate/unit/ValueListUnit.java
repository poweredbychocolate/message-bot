package com.crystalofchocolate.unit;

import com.crystalofchocolate.types.ValuesListEntity;

import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * The type Value list runnable unit.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-23
 */
public class ValueListUnit implements Runnable {
   private final ValuesListEntity valuesListEntity;
   private final ChannelUnit channelUnit;

   private List<String> channelsToUse;
   private List<String> randomMessage;
   private final Random random = new Random();

   /**
    * Instantiates a new Value list unit.
    *
    * @param valuesListEntity the values list entity
    * @param channelUnit      the channel unit
    */
   public ValueListUnit(ValuesListEntity valuesListEntity, ChannelUnit channelUnit) {
      this.valuesListEntity = valuesListEntity;
      this.channelUnit = channelUnit;
      initChannelToUseList();
      initRandomMessage();
   }

   /**
    * Scheduled period value.
    *
    * @return the period value
    */
   public long scheduledPeriod() {
      return valuesListEntity.getPeriod();
   }

   /**
    * Scheduled period value unit.
    *
    * @return the {@link TimeUnit}
    */
   public TimeUnit scheduledPeriodUnit() {
     return EntityPeriodUnit.asTimeUnit(valuesListEntity.getUnit());
   }

   @Override
   public void run() {
      String message = getRandomMessage();

      if (channelsToUse.isEmpty()) {
         channelUnit.sendMessage(message);
      } else {
         Optional<String> optionalChannelStringValue = getRandomChannelFromUserList();
         optionalChannelStringValue.ifPresent(channelStringValue -> {
            try {
               long parsedChannelId = Long.parseLong(channelStringValue);
               //send message to channel with given id
               channelUnit.sendMessage(parsedChannelId, message);
            } catch (NumberFormatException e) {
               //if channel is not id find channel by name
               channelUnit.sendMessage(channelStringValue, message);
            }
         });
      }
   }

   private String getRandomMessage() {
      if (randomMessage.isEmpty()) {
         initRandomMessage();
      }

      String message = randomMessage.get(random.nextInt(randomMessage.size()));
      randomMessage.remove(message);
      return message;
   }

   private Optional<String> getRandomChannelFromUserList() {
      if (channelsToUse.isEmpty()) {
         initChannelToUseList();
      }
      if (!channelsToUse.isEmpty()) {
         String channel = channelsToUse.get(random.nextInt(channelsToUse.size()));
         channelsToUse.remove(channel);
         return Optional.of(channel);
      }
      return Optional.empty();
   }

   private void initRandomMessage() {
      randomMessage = Objects.nonNull(valuesListEntity.getValues())
            ? new ArrayList<>(valuesListEntity.getValues())
            : new ArrayList<>(Collections.singletonList(" ? "));
   }

   private void initChannelToUseList() {
      channelsToUse = Objects.nonNull(valuesListEntity.getChannels())
            ? new ArrayList<>(valuesListEntity.getChannels())
            : new ArrayList<>(Collections.emptyList());
   }
}
