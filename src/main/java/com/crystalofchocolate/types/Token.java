package com.crystalofchocolate.types;

import lombok.extern.log4j.Log4j2;

import java.util.Objects;

/**
 * The type Token, discord apps token wrapper.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-12
 */
@Log4j2
public class Token {
   private final String tokenString;

   /**
    * Instantiates a new Token.
    *
    * @param tokenString the token string
    */
   public Token(String tokenString) {
      if (Objects.isNull(tokenString) || tokenString.isEmpty()) {
         log.error("Given value is not discord app token");
         throw new RuntimeException("Given value is not discord app token");
      }
      this.tokenString = tokenString;
   }

   /**
    * Get token string.
    *
    * @return the token string
    */
   public String get() {
      return tokenString;
   }

}
