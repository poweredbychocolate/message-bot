package com.crystalofchocolate.types;

import lombok.extern.log4j.Log4j2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * The File name representation.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-12
 */
@Log4j2
public class FileName {
   private final String fileNameString;

   /**
    * Instantiates a new File name.
    *
    * @param fileNameString the file name string
    */
   public FileName(String fileNameString) {
      if (!nameIsValid(fileNameString)) {
         log.error("Incorrect file name = " + fileNameString);
         throw new RuntimeException("Incorrect file name = " + fileNameString);
      }
      this.fileNameString = fileNameString;
   }

   private boolean nameIsValid(String name) {
      return Objects.nonNull(name) && !name.isEmpty() && Files.exists(Paths.get(name));
   }

   /**
    * Get as associate file reader.
    *
    * @return the reader
    * @throws FileNotFoundException the file not found exception
    */
   public Reader asFileReader() throws FileNotFoundException {
      return new FileReader(fileNameString);
   }
}
