package com.crystalofchocolate.types;

import com.crystalofchocolate.unit.EntityPeriodUnit;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The type Dictionary entity.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-23
 */
@Value
@EqualsAndHashCode
@Log4j2
public class DictionaryEntity {
   String name;
   Long period;
   String unit;
   List<String> channels;
   String title;
   Map<String, String> values;

   /**
    * Instantiates a new Dictionary entity.
    *
    * @param name     the name
    * @param period   the period
    * @param unit     the unit
    * @param channels the channels
    * @param title    the title
    * @param values   the values
    */
   public DictionaryEntity(String name, Long period, String unit, List<String> channels, String title, Map<String, String> values) {
      this.name = name;
      this.channels = channels;
      this.title = title;

      this.period = period;
      if (Objects.isNull(period) || period < 1) {
         log.error("Dictionary period value is not correct");
         throw new RuntimeException("Dictionary period value is not correct");
      }

      this.unit = unit;
      if (Objects.isNull(unit) || !EntityPeriodUnit.validate(unit)) {
         log.error("Dictionary period value unit is not correct");
         throw new RuntimeException("Dictionary period value unit is not correct");
      }

      this.values = values;
      if (Objects.isNull(this.values) || this.values.isEmpty()) {
         log.error("Values List, list of values is empty");
         throw new RuntimeException("Values List, list of values is empty");

      }
   }
}
