package com.crystalofchocolate.types;


import lombok.extern.log4j.Log4j2;

import java.util.Optional;

/**
 * The type Message command for call send message by command.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-12
 */
@Log4j2
public class MessageCommand {
   //first element is empty
   private final String[] parts;

   /**
    * Instantiates a new Message command.
    *
    * @param command the command
    */
   public MessageCommand(String command) {
      if (!command.startsWith("#")) {
         log.error("Unknown command");
         throw new RuntimeException("Unknown command");
      }
      parts = command.split("#");
   }

   /**
    * Get channel id or name if present.
    *
    * @return the optional {@link String}
    */
   public Optional<String> channel() {
      if (parts.length == 3) {
         return Optional.of(parts[1]);
      }
      return Optional.empty();
   }

   /**
    * Get message content.
    *
    * @return the string content
    */
   public String message() {
      if (parts.length == 3) {
         return parts[2];
      }
      return parts[1];
   }
}
