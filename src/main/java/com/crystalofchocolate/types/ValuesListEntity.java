package com.crystalofchocolate.types;

import com.crystalofchocolate.unit.EntityPeriodUnit;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Objects;

/**
 * The type Values list entity, mapping json file to object.
 *
 * @author Dawid
 * @version 1
 * @since 2020-07-19
 */
@Value
@EqualsAndHashCode
@Log4j2
public class ValuesListEntity {
   String name;
   Long period;
   String unit;
   List<String> channels;
   List<String> values;

   public ValuesListEntity(String name, Long period, String unit, List<String> channels, List<String> values) {
      this.name = name;
      this.channels = channels;

      this.period = period;
      if (Objects.isNull(this.period) || this.period < 1) {
         log.error("Values List period value is not correct");
         throw new RuntimeException("Values List period value is not correct");
      }

      this.unit = unit;
      if (Objects.isNull(this.unit) || !EntityPeriodUnit.validate(this.unit)) {
         log.error("Values List period value unit is not correct");
         throw new RuntimeException("Values List period value unit is not correct");
      }


      this.values = values;
      if (Objects.isNull(this.values) || this.values.isEmpty()) {
         log.error("Values List, list of values is empty");
         throw new RuntimeException("Values List, list of values is empty");
      }
   }

}
