package com.crystalofchocolate.listeners;

import com.crystalofchocolate.unit.ChannelUnit;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class ChannelMessageTest {

   @Test
   void sendMessages(){
      ChannelUnit channelUnit = mock(ChannelUnit.class);
      doNothing().when(channelUnit).sendMessage(anyString());
      doNothing().when(channelUnit).sendMessage(anyString(),anyString());
      doNothing().when(channelUnit).sendMessage(anyLong(),anyString());

      ChannelMessage channelMessage = new ChannelMessage(channelUnit);
      channelMessage.send("test message");
      channelMessage.send("test channel","test message");
      channelMessage.send(462734387965432L,"test message");

   }


}