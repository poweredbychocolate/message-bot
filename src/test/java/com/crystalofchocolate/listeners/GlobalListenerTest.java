package com.crystalofchocolate.listeners;

import org.javacord.api.event.message.MessageCreateEvent;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class GlobalListenerTest {

   @Test
   void onMessageCreate() {
      ChannelMessage channelMessage = mock(ChannelMessage.class);
      doNothing().when(channelMessage).send(anyString());
      doNothing().when(channelMessage).send(anyString(), anyString());
      doNothing().when(channelMessage).send(anyLong(), anyString());

      MessageCreateEvent messageCreateEvent1 = mock(MessageCreateEvent.class);
      doReturn(true).when(messageCreateEvent1).isPrivateMessage();
      doReturn("#test message").when(messageCreateEvent1).getMessageContent();

      MessageCreateEvent messageCreateEvent2 = mock(MessageCreateEvent.class);
      doReturn(true).when(messageCreateEvent2).isPrivateMessage();
      doReturn("#Test chanel#test message").when(messageCreateEvent2).getMessageContent();

      MessageCreateEvent messageCreateEvent3 = mock(MessageCreateEvent.class);
      doReturn(true).when(messageCreateEvent3).isPrivateMessage();
      doReturn("#5985654525#test message").when(messageCreateEvent3).getMessageContent();

      GlobalListener globalListener = new GlobalListener(channelMessage);
      globalListener.onMessageCreate(messageCreateEvent1);
      globalListener.onMessageCreate(messageCreateEvent2);
      globalListener.onMessageCreate(messageCreateEvent3);

   }
}