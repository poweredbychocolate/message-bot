package com.crystalofchocolate.unit;

import org.javacord.api.entity.channel.ServerTextChannel;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ChannelUnitTest {

   @Test
   void test() {

      ServerTextChannel serverTextChannel1 = mock(ServerTextChannel.class);
      ServerTextChannel serverTextChannel2 = mock(ServerTextChannel.class);
      List<Optional<ServerTextChannel>> channels =
            Arrays.asList(Optional.of(serverTextChannel1), Optional.of(serverTextChannel2));

      when(serverTextChannel1.sendMessage(anyString())).thenReturn(null);
      when(serverTextChannel2.sendMessage(anyString())).thenReturn(null);

      when(serverTextChannel1.getName()).thenReturn("CH1");
      when(serverTextChannel2.getName()).thenReturn("CH2");

//      when(serverTextChannel1.getName().equals(anyString())).thenReturn(true);
//      when(serverTextChannel2.getName().equals(anyString())).thenReturn(true);

      ChannelUnit channelUnit = new ChannelUnit(channels);

      channelUnit.sendMessage("Test message1");
      channelUnit.sendMessage("Test message2");
      channelUnit.sendMessage("Test message3");
      channelUnit.sendMessage("Test message4");
      channelUnit.sendMessage("Test message5");

      channelUnit.sendMessage(321312L, "Test message1");
      channelUnit.sendMessage(4432L, "Test message2");
      channelUnit.sendMessage(54234L, "Test message3");
      channelUnit.sendMessage(342342423L, "Test message4");
      channelUnit.sendMessage(43432432L, "Test message5");

      channelUnit.sendMessage("CH1", "Test message1");
      channelUnit.sendMessage("CH2", "Test message2");
      channelUnit.sendMessage("CH1", "Test message3");
      channelUnit.sendMessage("CH1", "Test message4");
      channelUnit.sendMessage("CH2", "Test message5");


   }

}