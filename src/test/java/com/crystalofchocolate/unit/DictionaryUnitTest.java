package com.crystalofchocolate.unit;

import com.crystalofchocolate.types.DictionaryEntity;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DictionaryUnitTest {

   @Test
   void test() {
      ServerTextChannel serverTextChannel1 = mock(ServerTextChannel.class);
      ServerTextChannel serverTextChannel2 = mock(ServerTextChannel.class);
      DictionaryEntity dictionaryEntity1 = mock(DictionaryEntity.class);
      DictionaryEntity dictionaryEntity2 = mock(DictionaryEntity.class);

      List<Optional<ServerTextChannel>> channels =
            Arrays.asList(Optional.of(serverTextChannel1), Optional.of(serverTextChannel2));

      when(serverTextChannel1.sendMessage(anyString())).thenReturn(null);
      when(serverTextChannel2.sendMessage(anyString())).thenReturn(null);
      when(serverTextChannel1.getName()).thenReturn("CH1");
      when(serverTextChannel2.getName()).thenReturn("CH2");

      when(dictionaryEntity1.getPeriod()).thenReturn(1L);
      when(dictionaryEntity1.getUnit()).thenReturn("minutes");
      when(dictionaryEntity1.getValues()).thenReturn(new HashMap<String, String>() {{
         put("key1", "value1");
         put("key2", "value2");
      }});
      when(dictionaryEntity1.getChannels()).thenReturn(Arrays.asList("CH1","CH2","232342342"));
      when(dictionaryEntity1.getTitle()).thenReturn("Test Title");

      when(dictionaryEntity2.getPeriod()).thenReturn(1L);
      when(dictionaryEntity2.getUnit()).thenReturn("minutes");
      when(dictionaryEntity2.getValues()).thenReturn(null);
      when(dictionaryEntity2.getChannels()).thenReturn(null);
      when(dictionaryEntity2.getTitle()).thenReturn(null);

      ChannelUnit channelUnit = new ChannelUnit(channels);
      DictionaryUnit dictionaryUnit1 = new DictionaryUnit(dictionaryEntity1, channelUnit);
      DictionaryUnit dictionaryUnit2 = new DictionaryUnit(dictionaryEntity2, channelUnit);

      assertEquals(1, dictionaryUnit1.scheduledPeriod());
      assertEquals(TimeUnit.MINUTES, dictionaryUnit1.scheduledPeriodUnit());
      assertDoesNotThrow(dictionaryUnit1::run);
      assertDoesNotThrow(dictionaryUnit1::run);
      assertDoesNotThrow(dictionaryUnit1::run);
      assertDoesNotThrow(dictionaryUnit1::run);
      assertDoesNotThrow(dictionaryUnit1::run);

      assertDoesNotThrow(dictionaryUnit2::run);
      assertDoesNotThrow(dictionaryUnit2::run);
      assertDoesNotThrow(dictionaryUnit2::run);
      assertDoesNotThrow(dictionaryUnit2::run);
      assertDoesNotThrow(dictionaryUnit2::run);
   }
}