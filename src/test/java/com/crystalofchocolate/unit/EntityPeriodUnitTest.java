package com.crystalofchocolate.unit;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class EntityPeriodUnitTest {

   @Test
   void validate() {
      assertFalse(EntityPeriodUnit.validate("null"));
      assertFalse(EntityPeriodUnit.validate("year"));
      assertFalse(EntityPeriodUnit.validate("years"));

      assertTrue(EntityPeriodUnit.validate("minutes"));
      assertTrue(EntityPeriodUnit.validate("minute"));
      assertTrue(EntityPeriodUnit.validate("hour"));
      assertTrue(EntityPeriodUnit.validate("hours"));
      assertTrue(EntityPeriodUnit.validate("second"));
      assertTrue(EntityPeriodUnit.validate("seconds"));
      assertTrue(EntityPeriodUnit.validate("day"));
      assertTrue(EntityPeriodUnit.validate("days"));
   }

   @Test
   void asTimeUnit() {
      assertNotNull(EntityPeriodUnit.asTimeUnit("null"));
      assertNotNull(EntityPeriodUnit.asTimeUnit("year"));
      assertNotNull(EntityPeriodUnit.asTimeUnit("years"));

      assertEquals(TimeUnit.MINUTES, EntityPeriodUnit.asTimeUnit("null"));
      assertEquals(TimeUnit.MINUTES, EntityPeriodUnit.asTimeUnit("year"));
      assertEquals(TimeUnit.MINUTES, EntityPeriodUnit.asTimeUnit("years"));

      assertEquals(TimeUnit.MINUTES, EntityPeriodUnit.asTimeUnit("minutes"));
      assertEquals(TimeUnit.MINUTES, EntityPeriodUnit.asTimeUnit("minute"));
      assertEquals(TimeUnit.HOURS, EntityPeriodUnit.asTimeUnit("hour"));
      assertEquals(TimeUnit.HOURS, EntityPeriodUnit.asTimeUnit("hours"));
      assertEquals(TimeUnit.SECONDS, EntityPeriodUnit.asTimeUnit("second"));
      assertEquals(TimeUnit.SECONDS, EntityPeriodUnit.asTimeUnit("seconds"));
      assertEquals(TimeUnit.DAYS, EntityPeriodUnit.asTimeUnit("day"));
      assertEquals(TimeUnit.DAYS, EntityPeriodUnit.asTimeUnit("days"));
   }
}