package com.crystalofchocolate.unit;

import com.crystalofchocolate.types.ValuesListEntity;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ValueListUnitTest {

   @Test
   void test() {
      ServerTextChannel serverTextChannel1 = mock(ServerTextChannel.class);
      ServerTextChannel serverTextChannel2 = mock(ServerTextChannel.class);
      ValuesListEntity valuesListEntity1 = mock(ValuesListEntity.class);
      ValuesListEntity valuesListEntity2 = mock(ValuesListEntity.class);

      List<Optional<ServerTextChannel>> channels =
            Arrays.asList(Optional.of(serverTextChannel1), Optional.of(serverTextChannel2));

      when(serverTextChannel1.sendMessage(anyString())).thenReturn(null);
      when(serverTextChannel2.sendMessage(anyString())).thenReturn(null);
      when(serverTextChannel1.getName()).thenReturn("CH1");
      when(serverTextChannel2.getName()).thenReturn("CH2");

      when(valuesListEntity1.getPeriod()).thenReturn(1L);
      when(valuesListEntity1.getUnit()).thenReturn("minutes");
      when(valuesListEntity1.getValues()).thenReturn(Arrays.asList("key1", "value1","key2", "value2"));
      when(valuesListEntity1.getChannels()).thenReturn(Arrays.asList("CH1", "CH2", "232342342"));

      when(valuesListEntity2.getPeriod()).thenReturn(1L);
      when(valuesListEntity2.getUnit()).thenReturn("minutes");
      when(valuesListEntity2.getValues()).thenReturn(null);
      when(valuesListEntity2.getChannels()).thenReturn(null);

      ChannelUnit channelUnit = new ChannelUnit(channels);
      ValueListUnit valueListUnit1 = new ValueListUnit(valuesListEntity1, channelUnit);
      ValueListUnit valueListUnit2 = new ValueListUnit(valuesListEntity2, channelUnit);

      assertEquals(1, valueListUnit1.scheduledPeriod());
      assertEquals(TimeUnit.MINUTES, valueListUnit1.scheduledPeriodUnit());
      assertDoesNotThrow(valueListUnit1::run);
      assertDoesNotThrow(valueListUnit1::run);
      assertDoesNotThrow(valueListUnit1::run);
      assertDoesNotThrow(valueListUnit1::run);
      assertDoesNotThrow(valueListUnit1::run);

      assertDoesNotThrow(valueListUnit2::run);
      assertDoesNotThrow(valueListUnit2::run);
      assertDoesNotThrow(valueListUnit2::run);
      assertDoesNotThrow(valueListUnit2::run);
      assertDoesNotThrow(valueListUnit2::run);
   }
}