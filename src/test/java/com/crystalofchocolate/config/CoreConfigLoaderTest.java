package com.crystalofchocolate.config;

import com.crystalofchocolate.loaders.CoreConfigLoader;
import com.crystalofchocolate.types.FileName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoreConfigLoaderTest {

   @Test
   void loadCoreConfig() {
      FileName fileName = new FileName("src/test/resources/core-conf.json");
      assertDoesNotThrow(() -> {
         CoreConfig coreConfig = new CoreConfigLoader().loadCoreConfig(fileName);
         assertNotNull(coreConfig);
         assertEquals(coreConfig.token(), "NzAxBzwwODMwMzAzMTY25DY2.Xpv8uw.7rrw7PpsgxeL2dhzVJ2WVuvHOFg");
         assertNull(coreConfig.dictionaryFilesPath());
         assertNull(coreConfig.listFilesPath());
      });

      FileName fileName2 = new FileName("src/test/resources/core-conf2.json");
      assertDoesNotThrow(() -> {
         CoreConfig coreConfig = new CoreConfigLoader().loadCoreConfig(fileName2);
         assertNotNull(coreConfig);
         assertEquals(coreConfig.token(), "NzAxBzwwODMwMzAzMTY25DY2.Xpv8uw.7rrw7PpsgxeL2dhzVJ2WVuvHOFg");
         assertNotNull(coreConfig.dictionaryFilesPath());
         assertNotNull(coreConfig.listFilesPath());
         assertEquals(1, coreConfig.dictionaryFilesPath().size());
         assertEquals(1, coreConfig.listFilesPath().size());
         assertEquals("example-dictionary.json", coreConfig.dictionaryFilesPath().get(0));
         assertEquals("example-list.json", coreConfig.listFilesPath().get(0));
      });
   }
}