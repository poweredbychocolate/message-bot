package com.crystalofchocolate.config;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ConfigFileTest {

   @Test
   void test() {
      ConfigFile configFile = new ConfigFile();
      configFile.setToken("test token");
      configFile.setEnabled_dictionary_files(Collections.singletonList("no-dictionary.empty"));
      configFile.setEnabled_list_files(Collections.singletonList("no-list.empty"));

      assertNotNull(configFile.getEnabled_dictionary_files());
      assertNotNull(configFile.getEnabled_list_files());
      assertFalse(configFile.getToken().isEmpty());

      CoreConfig coreConfig = configFile.as();
      assertNotNull(configFile);
      assertEquals("test token", coreConfig.token());
      assertEquals(1, coreConfig.dictionaryFilesPath().size());
      assertEquals(1, coreConfig.listFilesPath().size());
      assertEquals("no-dictionary.empty", coreConfig.dictionaryFilesPath().get(0));
      assertEquals("no-list.empty", coreConfig.listFilesPath().get(0));
   }
}