package com.crystalofchocolate.loaders;

import com.crystalofchocolate.types.DictionaryEntity;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DictionaryEntityLoaderTest {
   @Test
   void test() {
      List<String> patches = Collections.singletonList("src/test/resources/example-dictionary.json");
      List<DictionaryEntity> list = new DictionaryEntityLoader().loadDictionaryEntries(patches);

      assertEquals(1, list.size());
      assertEquals("english world of the day", list.get(0).getTitle());
      assertEquals(6, list.get(0).getValues().size());

      assertEquals(0, new DictionaryEntityLoader().loadDictionaryEntries(Collections.emptyList()).size());
      assertNull(new DictionaryEntityLoader().loadDictionaryEntity(""));
   }

}