package com.crystalofchocolate.loaders;

import com.crystalofchocolate.types.ValuesListEntity;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValuesListEntityLoaderTest {

   @Test
   void test() {
      List<String> patches = Collections.singletonList("src/test/resources/example-list.json");
      List<ValuesListEntity> list = new ValuesListEntityLoader().loadValuesListEntries(patches);


      assertEquals(1, list.size());
      assertEquals("example list", list.get(0).getName());
      assertEquals(5, list.get(0).getValues().size());

      assertEquals(0, new ValuesListEntityLoader().loadValuesListEntries(Collections.emptyList()).size());
      assertNull(new ValuesListEntityLoader().loadValuesListEntity(""));
   }
}