package com.crystalofchocolate.types;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileNameTest {

   @Test
   void test(){
      assertThrows(RuntimeException.class,() -> new FileName(null));
      assertThrows(RuntimeException.class,() -> new FileName(""));
      assertThrows(RuntimeException.class,() -> new FileName("2dhzVJ2WV"));
      assertDoesNotThrow(() -> new FileName("src/test/resources/core-conf.json"));
   }
}