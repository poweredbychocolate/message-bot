package com.crystalofchocolate.types;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class ValuesListEntityTest {
   @Test
   void test() {
      String name = "test list";
      Long period = 10L;
      String unit = "minutes";
      List<String> channels = Collections.singletonList("test-channel-2");
      List<String> values = Arrays.asList(
            "small cat",
            "big cat",
            "left direction",
            "right direction",
            "man",
            "woman"
      );


      ValuesListEntity valuesListEntity = new ValuesListEntity(name, period, unit, channels, values);

      assertEquals(name, valuesListEntity.getName());
      assertEquals(period, valuesListEntity.getPeriod());
      assertEquals(unit, valuesListEntity.getUnit());
      assertEquals(channels, valuesListEntity.getChannels());
      assertEquals(values, valuesListEntity.getValues());

      assertEquals(new ValuesListEntity(name, period, unit, channels, values).toString(), valuesListEntity.toString());
      assertEquals(valuesListEntity, new ValuesListEntity(name, period, unit, channels, values));

      assertThrows(RuntimeException.class, () -> {
         new ValuesListEntity(name, null, unit, channels, values);
      });

      assertThrows(RuntimeException.class, () -> {
         new ValuesListEntity(name, -39L, unit, channels, values);
      });

      assertThrows(RuntimeException.class, () -> {
         new ValuesListEntity(name, period, null, channels, values);
      });

      assertThrows(RuntimeException.class, () -> {
         new ValuesListEntity(name, period, "years", channels, values);
      });

      assertThrows(RuntimeException.class, () -> {
         new ValuesListEntity(name, period, unit, channels, null);
      });

      assertThrows(RuntimeException.class, () -> {
         new ValuesListEntity(name, period, unit, channels, Collections.emptyList());
      });
   }

}