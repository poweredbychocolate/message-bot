package com.crystalofchocolate.types;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class DictionaryEntityTest {
   @Test
   void test() {
      String name = "test dictionary";
      Long period = 10L;
      String unit = "minutes";
      List<String> channels = Collections.singletonList("test-channel-2");
      String title = "test title";
      Map<String, String> values = new HashMap<String, String>() {
         {
            put("small cat", "big cat");
            put("left direction", "right direction");
            put("man", "woman");
         }
      };


      DictionaryEntity dictionaryEntity = new DictionaryEntity(name, period, unit, channels, title, values);

      assertEquals(name, dictionaryEntity.getName());
      assertEquals(period, dictionaryEntity.getPeriod());
      assertEquals(unit, dictionaryEntity.getUnit());
      assertEquals(channels, dictionaryEntity.getChannels());
      assertEquals(title, dictionaryEntity.getTitle());
      assertEquals(values, dictionaryEntity.getValues());

      assertEquals(new DictionaryEntity(name, period, unit, channels, title, values).toString(), dictionaryEntity.toString());
      assertEquals(dictionaryEntity, new DictionaryEntity(name, period, unit, channels, title, values));

      assertThrows(RuntimeException.class, () -> {
         new DictionaryEntity(name, null, unit, channels, title, values);
      });

      assertThrows(RuntimeException.class, () -> {
         new DictionaryEntity(name, -98L, unit, channels, title, values);
      });

      assertThrows(RuntimeException.class, () -> {
         new DictionaryEntity(name, period, null, channels, title, values);
      });

      assertThrows(RuntimeException.class, () -> {
         new DictionaryEntity(name, period, "null", channels, title, values);
      });

      assertThrows(RuntimeException.class, () -> {
         new DictionaryEntity(name, period, unit, channels, title, null);
      });

      assertThrows(RuntimeException.class, () -> {
         new DictionaryEntity(name, period, unit, channels, title, Collections.emptyMap());
      });
   }

}