package com.crystalofchocolate.types;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageCommandTest {

   @Test
   void test() {

      assertThrows(RuntimeException.class, () -> new MessageCommand("test"));
      assertDoesNotThrow(() -> new MessageCommand("#test"));
      assertDoesNotThrow(() -> {
         MessageCommand command = new MessageCommand("#45563435435345#this is test message");
         assertTrue(command.channel().isPresent());
         assertEquals("45563435435345", command.channel().get());
         assertEquals("this is test message",command.message());
      });
      assertDoesNotThrow(() -> {
         MessageCommand command = new MessageCommand("#this is test2 message2");
         assertFalse(command.channel().isPresent());
         assertEquals("this is test2 message2",command.message());

      });
   }

}