package com.crystalofchocolate.types;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TokenTest {
   @Test
   void test(){
      assertThrows(RuntimeException.class,() -> new Token(null));
      assertThrows(RuntimeException.class,() -> new Token(""));
      assertDoesNotThrow(() -> new Token("2dhzVJ2WV"));
   }

}